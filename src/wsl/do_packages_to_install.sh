#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : do_packages_to_install.sh                                     ##
##  Project   : my_computer_tidy_and_clean                                    ##
##  Date      : Aug 28, 2020                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt 2020                                                  ##
##                                                                            ##
##  Description :                                                             ##
##                                                                            ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
source /usr/local/src/stdmatt/shellscript_utils/main.sh


##----------------------------------------------------------------------------##
## Variables                                                                  ##
##----------------------------------------------------------------------------##
SCRIPT_DIR=$(pw_get_script_dir);
CURR_OS="$(pw_os_get_simple_name)";


##----------------------------------------------------------------------------##
## Functions                                                                  ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
install_packages()
{
    ## @notice(stdmatt); We're assuming Debian based distros...
    local FILENAME="$1";
    while IFS= read -r LINE; do
        ## Ignore comments...
        if [ -n "$(pw_string_starts_with "$LINE" "--")" ]; then
            continue;
        fi;
        ## Ignore empty lines...
        if [ -z "$LINE" ]; then
            continue;
        fi;

        echo "$(pw_FG "--> Installing $LINE <--")";
        sudo apt-get install -y "$LINE";
    done < "$FILENAME";
}

##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
echo "$(pw_FM "--: Innstalling packages ($CURR_OS) :--")";

PACKAGE_GROUPS="";
## If no argumetns are given install everything under ./packages
if [ "$BASH_ARGC" == "0" ]; then
    PACKAGE_GROUPS=$(ls "${SCRIPT_DIR}/packages");
else
    PACKAGE_GROUPS="$@";
fi

sudo apt-get update -y && sudo apt-get upgrade -y
PACKAGE_GROUPS=$(echo "$PACKAGE_GROUPS" | sed s/\.txt//g); ## Remove the extensions.
for PACKAGE_GROUP in $PACKAGE_GROUPS; do
    echo "$(pw_FB "--> PACKAGE GROUP: $PACKAGE_GROUP <--")";
    install_packages "${SCRIPT_DIR}/packages/${PACKAGE_GROUP}.txt"
done;
