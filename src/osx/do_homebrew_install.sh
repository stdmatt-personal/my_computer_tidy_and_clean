#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : do_homebrew_install.sh                                        ##
##  Project   : my_computer_tidy_and_clean                                    ##
##  Date      : Oct 08, 2019                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt 2020                                                  ##
##                                                                            ##
##  Description :                                                             ##
##                                                                            ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Casks / Formulaes                                                          ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
CASKS="android-file-transfer \
       appcleaner            \
       chromium              \
       firefox               \
       gimp                  \
       google-chrome         \
       vlc                   \
       meld                  \
       oracle-jdk            \
       osxfuse               \
       pycharm-ce            \
       disk-inventory-x      \
       xquartz               \
       telegram-desktop      \
       tor-browser           \
       wine-stable           \
       visual-studio-code    \
       font-jetbrains-mono";


##------------------------------------------------------------------------------
FORMULAES="git         \
           git-gui     \
           node        \
           ghostscript \
           youtube-dl  \
           graphviz    \
           sshfs       \
           cmake       \
           sdl2        \
           sdl2_image  \
           sdl2_ttf    \
           sdl2_mixer  \
           sdl2_net    \
           sdl2_gfx    \
           atool       \
           unrar       \
           dosbox      \
           ";

##------------------------------------------------------------------------------
CASKROOMS="Caskroom/cask/wkhtmltopdf";


##----------------------------------------------------------------------------##
## Functions                                                                  ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
ensure_homebrew()
{
    local BREW=$(which brew);
    if [ -z "$BREW" ]; then
        /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)";
    fi;

    brew upgrade;
}

##------------------------------------------------------------------------------
check_if_installed()
{
    if brew ls --versions myformula > /dev/null; then
        echo "true";
    else
        echo "false";
    fi
}

##------------------------------------------------------------------------------
install_formulaes()
{
    for FORMULA in $FORMULAES; do
        local IS_INSTALLED=$(check_if_installed "$FORMULA");
        if [ "$IS_INSTALLED" ]; then
            echo "($FORMULA) is already installed - Skipping...";
        else
            brew install "$FORMULA";
        fi;
    done;
}

##------------------------------------------------------------------------------
install_casks()
{
    for CASK in $CASKS; do
        local IS_INSTALLED=$(check_if_installed "$CASK");
        if [ "$IS_INSTALLED" ]; then
            echo "($CASK) is already installed - Skipping...";
        else
            brew cask install "$CASK";
        fi;
    done;
}

##------------------------------------------------------------------------------
install_casksrooms()
{
    for CASKROOM in $CASKROOMS; do
        local IS_INSTALLED=$(check_if_installed "$CASKROOM");
        if [ "$IS_INSTALLED" ]; then
            echo "($CASKROOM) is already installed - Skipping...";
        else
            brew install "$CASKROOM";
        fi;
    done;
}


##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
ensure_homebrew;
brew tap homebrew/cask-fonts ## @Hack:: Find a better place for this.

install_casks;
install_formulaes;
install_casksrooms;
