#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : install.sh                                                    ##
##  Project   : my_computer_tidy_and_clean                                    ##
##  Date      : Jan 09, 2020                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt 2020                                                  ##
##                                                                            ##
##  Description :                                                             ##
##                                                                            ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Functions                                                                  ##
##----------------------------------------------------------------------------##
_get_script_dir()
{
   local SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[1]}")" && pwd)";
   echo "$SCRIPT_DIR"
}

##----------------------------------------------------------------------------##
## Variables                                                                  ##
##----------------------------------------------------------------------------##
SCRIPT_DIR="$(_get_script_dir)";
DIR_BIN="/usr/local/bin/stdmatt/my_computer_tidy_and_clean";
ENTRY_POINT_FILENAME="/usr/local/bin/my-computer-tidy-and-clean";

##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
if [ "$UID" != "0" ]; then
   echo "Needs elevated privileges to install";
   exit 1;
fi;

## Clean the installation directory
rm    -rf "${DIR_BIN}";
mkdir -p  "${DIR_BIN}";

## Copy the files.
cp -R "${SCRIPT_DIR}/src" "${DIR_BIN}";
## rm -rf "${DIR_BIN}/.git";

## Make the files executable.
find "${DIR_BIN}" -exec chmod 755 {} \;

## Create a link into to the entry point.
rm    "$ENTRY_POINT_FILENAME";
touch "$ENTRY_POINT_FILENAME";

echo "#!/usr/bin/env bash"       >> "$ENTRY_POINT_FILENAME";
echo "${DIR_BIN}/src/run.sh \$@" >> "$ENTRY_POINT_FILENAME";
chmod 755 "$ENTRY_POINT_FILENAME";
